﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeAutomation.Models.IntegrationProxies
{
    public class VerifyLocationMessageResponse
    {
        public Guid? UserGuid { get; set; }
    }
}
