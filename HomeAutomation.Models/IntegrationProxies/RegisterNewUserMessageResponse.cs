﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeAutomation.Models.IntegrationProxies
{
    public class RegisterNewUserMessageResponse
    {
        public Guid? UserGuid { get; set; }
        public string UserKey { get; set; }
        public string Message { get; set; }
    }
}
