﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeAutomation.Models.Wrappers
{
    public class DashboardIndexViewModel
    {
        public DashboardIndexViewModel()
        {
            UserLocations = new List<UserLocationWrapper>();
        }

        public int LocationCount { get; set; }

        public int DevicesCount { get; set; }

        public int LocationsSharedFromOtherUsersCount { get; set; }

        public int TotalAvailableLocations
        {
            get
            {
                return LocationCount + LocationsSharedFromOtherUsersCount;
            }
        }

        public List<UserLocationWrapper> UserLocations { get; set; }
    }
}