﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeAutomation.Models.Wrappers
{
	public class UserLocationWrapper
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string LocationKey { get; set; }
		public string LocationURL { get; set; }
		public int Port { get; set; }
		public List<UserDeviceWrapper> UserDevices { get; set; }
	}
}