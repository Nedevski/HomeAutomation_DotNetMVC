﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HomeAutomation.Models.Enumeration;

namespace HomeAutomation.Models.Wrappers
{
	public class UserDeviceWrapper
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string LocalIP { get; set; }
		public int Port { get; set; }
		public DeviceType Type { get; set; }

		public int LocationId { get; set; }

		public string LocalAddress
		{
			get
			{
				int localPort = (Port == 0) ? 80 : Port;
				return string.Format("{0}:{1}", LocalIP, localPort);
			}
		}
	}
}