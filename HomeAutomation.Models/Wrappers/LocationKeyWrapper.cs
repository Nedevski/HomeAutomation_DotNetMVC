﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeAutomation.Models.Wrappers
{
	public class LocationKeyWrapper
	{
		public int LocationId { get; set; }
		public string LocationKey { get; set; }

		public string URL { get; set; }
		public int Port { get; set; }

		public string FullUrl
		{
			get
			{
				string baseUrl = URL.Trim();

				if (!baseUrl.StartsWith("http://") && !baseUrl.StartsWith("https://"))
				{
					baseUrl = string.Format("{0}{1}", "http://", baseUrl);
				}

				var uri = new Uri(baseUrl);
				baseUrl = uri.GetLeftPart(UriPartial.Authority);

				int thePort = (Port == 0) ? 80 : Port;
				return string.Format("{0}:{1}", baseUrl, thePort);
			}
		}
	}
}
