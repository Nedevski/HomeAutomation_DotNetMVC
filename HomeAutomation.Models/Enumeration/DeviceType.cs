﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeAutomation.Models.Enumeration
{
	public enum DeviceType
	{
		Sensor,
		Switch,
		Slider,
		TextSender
	}
}
