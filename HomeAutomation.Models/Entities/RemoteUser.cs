﻿using System;
using System.ComponentModel.DataAnnotations;
using HomeAutomation.Models.Account;

namespace HomeAutomation.Models.Entities
{
	public class RemoteUser : BaseEntity
	{
        [Required]
        public Guid RemoteUserGuid { get; set; }

        [Required]
		public string LocalUserId { get; set; }
		public virtual ApplicationUser LocalUser { get; set; }

		[Required]
		public int LocationId { get; set; }
		public Location Location { get; set; }
	}
}