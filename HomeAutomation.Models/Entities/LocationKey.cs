﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeAutomation.Models.Account;

namespace HomeAutomation.Models.Entities
{
	public class LocationKey : BaseEntity
	{
		[Required]
		public string Key { get; set; }
        
		[Required]
		public bool HasFullDeviceControl { get; set; }

		public bool IsVerified { get; set; }

		public string OwnerId { get; set; }
		public virtual ApplicationUser Owner { get; set; }

		public int LocationId { get; set; }
		public virtual Location Location { get; set; }

		public virtual List<DevicePermission> Permissions { get; set; }
	}
}
