﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using HomeAutomation.Models.Account;
using HomeAutomation.Models.Enumeration;

namespace HomeAutomation.Models.Entities
{
    public class Device : BaseEntity
    {
		public Device()
		{
			Permissions = new List<DevicePermission>();
		}

		[Required]
        public string Name { get; set; }

        public string Description { get; set; }

		[Required]
		public string LocalIP { get; set; }

		public int Port { get; set; }

		[Required]
		public DeviceType Type { get; set; }

        [Required]
        public string OwnerId { get; set; }
        public virtual ApplicationUser Owner { get; set; }

        public int LocationId { get; set; }
		public virtual Location Location { get; set; }

		public virtual List<DevicePermission> Permissions { get; set; }
	}
}