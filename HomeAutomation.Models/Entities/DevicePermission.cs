﻿using System.ComponentModel.DataAnnotations;

namespace HomeAutomation.Models.Entities
{
	public class DevicePermission : BaseEntity
	{
		[Required]
		public int LocationKeyId { get; set; }
		public LocationKey LocationKey { get; set; }

		[Required]
		public int DeviceId { get; set; }
		public Device Device { get; set; }
	}
}