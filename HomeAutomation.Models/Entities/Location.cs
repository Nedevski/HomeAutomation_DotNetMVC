﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;
using HomeAutomation.Models.Account;

namespace HomeAutomation.Models.Entities
{
    public class Location : BaseEntity
    {
        public Location()
        {
            LocationKeys = new List<LocationKey>();
            RemoteUsers = new List<RemoteUser>();
            Devices = new List<Device>();
        }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public string LocationURL { get; set; }

        public int Port { get; set; }

        [Required]
        public string MasterKey { get; set; }

        [Required]
        public string OwnerId { get; set; }
        public virtual ApplicationUser Owner { get; set; }

        public bool IsVerified { get; set; }

        public virtual List<Device> Devices { get; set; }
        public virtual List<RemoteUser> RemoteUsers { get; set; }
        public virtual List<LocationKey> LocationKeys { get; set; }

        [NotMapped]
        public string FullUrl
        {
            get
            {
                return string.Format("{0}:{1}", LocationURL, Port);
            }
        }
    }
}
