﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;
using HomeAutomation.Models;
using HomeAutomation.Models.Account;
using HomeAutomation.Models.Entities;

namespace HomeAutomation.DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public DbSet<Location> Locations { get; set; }
        public DbSet<LocationKey> LocationKeys { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<DevicePermission> DevicePermissions { get; set; }
        public DbSet<RemoteUser> RemoteUsers { get; set; }

		public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }

        public new void SaveChanges()
        {
            base.SaveChanges();
        }
	}
}
