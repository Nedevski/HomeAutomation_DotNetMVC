namespace HomeAutomation.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models.Account;
    using Models.Entities;
    internal sealed class Configuration : DbMigrationsConfiguration<HomeAutomation.DAL.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "HomeAutomation.DAL.ApplicationDbContext";
        }

        protected override void Seed(HomeAutomation.DAL.ApplicationDbContext context)
        {
            //var store = new UserStore<ApplicationUser>(context);
            //var manager = new UserManager<ApplicationUser>(store);

            //if (!context.Users.Any(u => u.Email == "nikola.nedevski@gmail.com"))
            //{
            //    var user = new ApplicationUser
            //    {
            //        UserName = "nikola.nedevski@gmail.com",
            //        Email = "nikola.nedevski@gmail.com",
            //        EmailConfirmed = true,
            //        DateRegistered = DateTime.UtcNow
            //    };

            //    manager.Create(user, "Passw0rd");
            //}

            //if (!context.Users.Any(u => u.Email == "testUser@gmail.com"))
            //{
            //    var user2 = new ApplicationUser
            //    {
            //        UserName = "testUser@gmail.com",
            //        Email = "testUser@gmail.com",
            //        EmailConfirmed = true,
            //        DateRegistered = DateTime.UtcNow
            //    };

            //    manager.Create(user2, "Passw0rd");
            //}

            //context.SaveChanges();

            //ApplicationUser mainUser = context.Users.SingleOrDefault(u => u.Email == "nikola.nedevski@gmail.com");
            //ApplicationUser secondaryUser = context.Users.SingleOrDefault(u => u.Email == "testUser@gmail.com");


            //if (!context.Locations.Any(l => l.Name == "Main Location"))
            //{
            //    var mainUserLoc = new Location()
            //    {
            //        DateCreated = DateTime.UtcNow,
            //        DateModified = DateTime.UtcNow,
            //        Name = "Main Location",
            //        LocationURL = "rpisystem.local.nedevski.com",
            //        Port = 80
            //    };

            //    context.Locations.Add(mainUserLoc);
            //    context.SaveChanges();

            //    var mainUserLocKey = new LocationKey()
            //    {
            //        DateCreated = DateTime.UtcNow,
            //        DateModified = DateTime.UtcNow,
            //        IsMasterKey = true,
            //        IsVerified = true,
            //        HasFullDeviceControl = true,
            //        OwnerId = mainUser.Id,
            //        Key = "eb3693625f2dc549c587870ee0e4606f",
            //        LocationId = mainUserLoc.Id
            //    };

            //    context.LocationKeys.Add(mainUserLocKey);
            //    context.SaveChanges();
            //}

            //if (!context.Locations.Any(l => l.Name == "Secondary Location"))
            //{
            //    var secondaryUserLoc = new Location()
            //    {
            //        DateCreated = DateTime.UtcNow,
            //        DateModified = DateTime.UtcNow,
            //        Name = "Secondary Location",
            //        LocationURL = "whatever url",
            //        Port = 80
            //    };

            //    context.Locations.Add(secondaryUserLoc);
            //    context.SaveChanges();

            //    var secondaryUserLocKey = new LocationKey()
            //    {
            //        DateCreated = DateTime.UtcNow,
            //        DateModified = DateTime.UtcNow,
            //        IsMasterKey = true,
            //        IsVerified = true,
            //        HasFullDeviceControl = true,
            //        OwnerId = secondaryUser.Id,
            //        Key = "whatever",
            //        LocationId = secondaryUserLoc.Id
            //    };

            //    context.LocationKeys.Add(secondaryUserLocKey);
            //    context.SaveChanges();
            //}

            //if (!context.Devices.Any(l => l.Name == "PrimaryUser Device"))
            //{
            //    var locId = context.Locations.SingleOrDefault(l => l.Name == "Main Location").Id;

            //    var device = new Device()
            //    {
            //        DateCreated = DateTime.UtcNow,
            //        DateModified = DateTime.UtcNow,
            //        Name = "PrimaryUser Device",
            //        LocalIP = "192.168.0.110",
            //        Port = 80,
            //        LocationId = locId,
            //        Type = Models.Enumeration.DeviceType.Switch
            //    };

            //    context.Devices.Add(device);
            //    context.SaveChanges();
            //}

            //if (!context.Devices.Any(l => l.Name == "SecondaryUser Device"))
            //{
            //    var locId = context.Locations.SingleOrDefault(l => l.Name == "Secondary Location").Id;

            //    var device = new Device()
            //    {
            //        DateCreated = DateTime.UtcNow,
            //        DateModified = DateTime.UtcNow,
            //        Name = "SecondaryUser Device",
            //        LocalIP = "invalid",
            //        Port = 80,
            //        LocationId = locId,
            //        Type = Models.Enumeration.DeviceType.Switch
            //    };

            //    context.Devices.Add(device);
            //    context.SaveChanges();
            //}
        }
    }
}
