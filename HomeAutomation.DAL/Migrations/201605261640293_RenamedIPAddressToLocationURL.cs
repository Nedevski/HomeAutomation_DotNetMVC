namespace HomeAutomation.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamedIPAddressToLocationURL : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "LocationURL", c => c.String(nullable: false));
            DropColumn("dbo.Locations", "IPAddress");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Locations", "IPAddress", c => c.String(nullable: false));
            DropColumn("dbo.Locations", "LocationURL");
        }
    }
}
