namespace HomeAutomation.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DevicePermissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationKeyId = c.Int(nullable: false),
                        DeviceId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LocationKeys", t => t.LocationKeyId)
                .ForeignKey("dbo.Devices", t => t.DeviceId)
                .Index(t => t.LocationKeyId)
                .Index(t => t.DeviceId);
            
            CreateTable(
                "dbo.Devices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        LocalIP = c.String(nullable: false),
                        Port = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        IPAddress = c.String(nullable: false),
                        Port = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LocationKeys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Key = c.String(nullable: false),
                        IsMasterKey = c.Boolean(nullable: false),
                        HasFullDeviceControl = c.Boolean(nullable: false),
                        IsVerified = c.Boolean(nullable: false),
                        OwnerId = c.String(maxLength: 128),
                        LocationId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .ForeignKey("dbo.AspNetUsers", t => t.OwnerId)
                .Index(t => t.OwnerId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.RemoteUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RemoteUserId = c.Int(nullable: false),
                        LocalUserId = c.String(nullable: false, maxLength: 128),
                        LocationId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.LocalUserId)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .Index(t => t.LocalUserId)
                .Index(t => t.LocationId);
            
            AddColumn("dbo.AspNetUsers", "DateRegistered", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DevicePermissions", "DeviceId", "dbo.Devices");
            DropForeignKey("dbo.DevicePermissions", "LocationKeyId", "dbo.LocationKeys");
            DropForeignKey("dbo.RemoteUsers", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.RemoteUsers", "LocalUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.LocationKeys", "OwnerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.LocationKeys", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.Devices", "LocationId", "dbo.Locations");
            DropIndex("dbo.RemoteUsers", new[] { "LocationId" });
            DropIndex("dbo.RemoteUsers", new[] { "LocalUserId" });
            DropIndex("dbo.LocationKeys", new[] { "LocationId" });
            DropIndex("dbo.LocationKeys", new[] { "OwnerId" });
            DropIndex("dbo.Devices", new[] { "LocationId" });
            DropIndex("dbo.DevicePermissions", new[] { "DeviceId" });
            DropIndex("dbo.DevicePermissions", new[] { "LocationKeyId" });
            DropColumn("dbo.AspNetUsers", "DateRegistered");
            DropTable("dbo.RemoteUsers");
            DropTable("dbo.LocationKeys");
            DropTable("dbo.Locations");
            DropTable("dbo.Devices");
            DropTable("dbo.DevicePermissions");
        }
    }
}
