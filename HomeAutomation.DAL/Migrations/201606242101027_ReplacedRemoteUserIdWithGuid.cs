namespace HomeAutomation.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReplacedRemoteUserIdWithGuid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RemoteUsers", "RemoteUserGuid", c => c.Guid(nullable: false));
            DropColumn("dbo.RemoteUsers", "RemoteUserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RemoteUsers", "RemoteUserId", c => c.Int(nullable: false));
            DropColumn("dbo.RemoteUsers", "RemoteUserGuid");
        }
    }
}
