namespace HomeAutomation.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedLocationEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Devices", "OwnerId", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Locations", "MasterKey", c => c.String(nullable: false));
            AddColumn("dbo.Locations", "OwnerId", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Locations", "IsVerified", c => c.Boolean(nullable: false));
            AlterColumn("dbo.DevicePermissions", "DateModified", c => c.DateTime());
            AlterColumn("dbo.Devices", "DateModified", c => c.DateTime());
            AlterColumn("dbo.Locations", "DateModified", c => c.DateTime());
            AlterColumn("dbo.LocationKeys", "DateModified", c => c.DateTime());
            AlterColumn("dbo.RemoteUsers", "DateModified", c => c.DateTime());
            CreateIndex("dbo.Devices", "OwnerId");
            CreateIndex("dbo.Locations", "OwnerId");
            AddForeignKey("dbo.Locations", "OwnerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Devices", "OwnerId", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.LocationKeys", "IsMasterKey");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LocationKeys", "IsMasterKey", c => c.Boolean(nullable: false));
            DropForeignKey("dbo.Devices", "OwnerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Locations", "OwnerId", "dbo.AspNetUsers");
            DropIndex("dbo.Locations", new[] { "OwnerId" });
            DropIndex("dbo.Devices", new[] { "OwnerId" });
            AlterColumn("dbo.RemoteUsers", "DateModified", c => c.DateTime(nullable: false));
            AlterColumn("dbo.LocationKeys", "DateModified", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Locations", "DateModified", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Devices", "DateModified", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DevicePermissions", "DateModified", c => c.DateTime(nullable: false));
            DropColumn("dbo.Locations", "IsVerified");
            DropColumn("dbo.Locations", "OwnerId");
            DropColumn("dbo.Locations", "MasterKey");
            DropColumn("dbo.Devices", "OwnerId");
        }
    }
}
