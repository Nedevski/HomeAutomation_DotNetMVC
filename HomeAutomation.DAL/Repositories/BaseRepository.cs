﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using HomeAutomation.Models;

namespace HomeAutomation.DAL.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        private DbContext context;

        public BaseRepository(ApplicationDbContext context)
        {
            if (context == null)
            {
                throw new InvalidOperationException("Please bind the DbContext inside the Ninject container.");
            }

            this.context = context;
        }

        protected DbSet<T> DbSet
        {
            get
            {
                return context.Set<T>();
            }
        }

        protected Database Database
        {
            get
            {
                return context.Database;
            }
        }

        public virtual IQueryable<T> GetAll()
        {
            return DbSet;
        }

        public virtual IQueryable<T> Where(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public virtual T GetSingle(Expression<Func<T, bool>> predicate)
        {
            return GetAll().SingleOrDefault(predicate);
        }

        public virtual T GetSingle(int id)
        {
            return GetAll().SingleOrDefault(e => e.Id == id);
        }

        public virtual T Add(T entity, bool withSave = true)
        {
            Add(entity);

            if (withSave)
            {
                SaveChanges();
            }

            return entity;
        }

        public virtual IEnumerable<T> Add(IEnumerable<T> entities, bool withSave = true)
        {
            foreach (var entity in entities)
            {
                Add(entity);
            }

            if (withSave)
            {
                SaveChanges();
            }

            return entities;
        }

        public virtual T Update(T entity, bool withSave = true)
        {
            SetToModified(entity);

            if (withSave)
            {
                SaveChanges();
            }

            return entity;
        }

        public virtual IEnumerable<T> Update(IEnumerable<T> entities, bool withSave = true)
        {
            foreach (T entity in entities)
            {
                SetToModified(entity);
            }

            if (withSave)
            {
                SaveChanges();
            }

            return entities;
        }

        public virtual T PermaDelete(int id, bool withSave = true)
        {
            var entity = DbSet.Find(id);

            if (entity == null)
            {
                return null;
            }

            AttachAndRemove(entity);

            if (withSave)
            {
                SaveChanges();
            }

            return entity;
        }

        public virtual T PermaDelete(T entity, bool withSave = true)
        {
            AttachAndRemove(entity);

            if (withSave)
            {
                SaveChanges();
            }

            return entity;
        }

        public virtual IEnumerable<T> PermaDelete(IEnumerable<T> entities, bool withSave = true)
        {
            foreach (var entity in entities)
            {
                AttachAndRemove(entity);
            }

            if (withSave)
            {
                SaveChanges();
            }

            return entities;
        }

        public int? SaveChanges()
        {
            try
            {
                return context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                HandleDbEntityValidationException(dbEx);

                return null;
            }
        }

        public async Task<int?> SaveChangesAsync()
        {
            try
            {
                return await context.SaveChangesAsync();
            }
            catch (DbEntityValidationException dbEx)
            {
                HandleDbEntityValidationException(dbEx);

                return null;
            }
        }

        private static void HandleDbEntityValidationException(DbEntityValidationException dbEx)
        {
            var exceptionBuilder = new StringBuilder(dbEx.Message);

            foreach (var validationErrors in dbEx.EntityValidationErrors)
            {
                foreach (var validationError in validationErrors.ValidationErrors)
                {
                    exceptionBuilder.AppendFormat(
                        "Class: {0}, Property: {1}, Error: {2} {3}",
                        validationErrors.Entry.Entity.GetType().FullName,
                        validationError.PropertyName,
                        validationError.ErrorMessage,
                        Environment.NewLine);
                }
            }

            throw new DbEntityValidationException(exceptionBuilder.ToString(), dbEx.EntityValidationErrors);
        }

        private DbEntityEntry GetDbEntityEntry(T entity)
        {
            return context.Entry<T>(entity);
        }

        private void Add(T entity)
        {
            DbEntityEntry dbEntityEntry = GetDbEntityEntry(entity);

            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
        }

        private void SetToModified(T entity)
        {
            DbEntityEntry dbEntityEntry = GetDbEntityEntry(entity);

            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }

            dbEntityEntry.State = EntityState.Modified;
        }

        private void AttachAndRemove(T entity)
        {
            DbEntityEntry dbEntityEntry = GetDbEntityEntry(entity);

            if (dbEntityEntry.State != EntityState.Deleted)
            {
                dbEntityEntry.State = EntityState.Deleted;
            }
            else
            {

                DbSet.Attach(entity);
                DbSet.Remove(entity);
            }
        }
    }
}
