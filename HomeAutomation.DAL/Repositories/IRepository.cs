﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using HomeAutomation.Models;

namespace HomeAutomation.DAL.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        IQueryable<T> GetAll();

        IQueryable<T> Where(Expression<Func<T, bool>> predicate);

        T GetSingle(Expression<Func<T, bool>> predicate);

        T GetSingle(int id);

        T Add(T entity, bool withSave = true);
        IEnumerable<T> Add(IEnumerable<T> entities, bool withSave = true);

        T Update(T entity, bool withSave = true);
        IEnumerable<T> Update(IEnumerable<T> entities, bool withSave = true);

        T PermaDelete(int id, bool withSave = true);
        T PermaDelete(T entity, bool withSave = true);
        IEnumerable<T> PermaDelete(IEnumerable<T> entities, bool withSave = true);

        int? SaveChanges();
    }
}
