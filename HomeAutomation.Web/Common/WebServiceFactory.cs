﻿using System.Web.Mvc;
using HomeAutomation.Services.Abstract;

namespace HomeAutomation.Common
{
    public class WebServiceFactory : IServiceFactory
    {
        public T GetService<T>() where T : class
        {
            var service = DependencyResolver.Current.GetService(typeof(T));
            return (T)service;
        }
    }
}