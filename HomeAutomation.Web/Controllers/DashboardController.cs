﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HomeAutomation.Models.Wrappers;
using Microsoft.AspNet.Identity;
using HomeAutomation.Services.Interfaces;
using HomeAutomation.Services.Abstract;
using HomeAutomation.Controllers.Abstract;

namespace HomeAutomation.Controllers
{
	[Authorize]
    public class DashboardController : BaseController
    {
		public DashboardController(IServiceHolder ss) : base(ss)
        {
		}

        // GET: Dashboard
        public ActionResult Index()
        {
			DashboardIndexViewModel viewModel = SS.DashboardService.PopulateViewModelForUser(User.Identity.GetUserId());

			ViewBag.UserId = User.Identity.GetUserId();

			return View(viewModel);
        }
		
		[HttpPost]
		[ValidateAntiForgeryToken]
		public JsonResult GetLocationKeysForUser(string userId)
		{
			List<LocationKeyWrapper> keysForUser = SS.LocationKeyService.GetLocationKeysForUser();

			return Json(keysForUser);
		}
	}
}