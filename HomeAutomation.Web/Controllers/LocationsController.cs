﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeAutomation.DAL;
using HomeAutomation.Models.Entities;
using HomeAutomation.ViewModels.Locations;
using Microsoft.AspNet.Identity;
using HomeAutomation.Services.Interfaces;
using HomeAutomation.Controllers.Abstract;
using HomeAutomation.Services.Abstract;

namespace HomeAutomation.Controllers
{
	[Authorize]
	public class LocationsController : BaseController
	{
		public LocationsController(IServiceHolder ss) : base(ss)
        {
		}
        

		// GET: Locations
		public ActionResult Index()
		{
			var locations = SS.LocationService.GetAllForUser();
			return View(locations.ToList());
		}

		// GET: Locations/Details/5
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			Location location = SS.LocationService.GetSingleForUser(id.Value);

			if (location == null)
			{
				return HttpNotFound();
			}

			return View(location);
		}

		// GET: Locations/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: Locations/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(AddEditLocationViewModel location)
		{
			if (!ModelState.IsValid)
			{
				return View(location);
			}

            Location locationToAdd = new Location()
            {
                Name = location.Name,
                Description = location.Description,
                LocationURL = location.LocationURL,
                Port = location.Port,
                MasterKey = location.MasterKey,
                IsVerified = false,
                OwnerId = CurrentUserId
            };

            SS.LocationService.Add(locationToAdd);

            return RedirectToAction("Index");
		}

		// GET: Locations/Edit/5
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			Location location = SS.LocationService.GetSingleForUser(id.Value);

			if (location == null)
			{
				return HttpNotFound();
			}

			AddEditLocationViewModel vm = new AddEditLocationViewModel(location);
			return View(vm);
		}

		// POST: Locations/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(AddEditLocationViewModel moddedLoc)
		{
			if (!ModelState.IsValid)
			{
				return View(moddedLoc);
			}

            Location existingLoc = SS.LocationService.GetSingleForUser(moddedLoc.Id);

            if (existingLoc.LocationURL != moddedLoc.LocationURL || existingLoc.Port != moddedLoc.Port || existingLoc.MasterKey != moddedLoc.MasterKey)
            {
                existingLoc.IsVerified = false;
            }

            existingLoc.Name = moddedLoc.Name;
            existingLoc.Description = moddedLoc.Description;
            existingLoc.LocationURL = moddedLoc.LocationURL;
            existingLoc.Port = moddedLoc.Port;
            existingLoc.MasterKey = moddedLoc.MasterKey;

            SS.LocationService.Update(existingLoc);

            return RedirectToAction("Index");
		}

        public ActionResult Verify(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Location location = SS.LocationService.GetSingleForUser(id.Value);

            if (location == null)
            {
                return HttpNotFound();
            }

            bool success = SS.LocationService.VerifyLocation(location);

            return Json(success, JsonRequestBehavior.AllowGet);
        }

		// GET: Locations/Delete/5
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			Location location = SS.LocationService.GetSingleForUser(id.Value);

			if (location == null)
			{
				return HttpNotFound();
			}

			return View(location);
		}

		// POST: Locations/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			SS.LocationService.Delete(id);

			return RedirectToAction("Index");
		}
	}
}
