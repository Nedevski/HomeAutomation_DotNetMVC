﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeAutomation.DAL;
using HomeAutomation.Models.Entities;
using HomeAutomation.ViewModels.Permissions;
using Microsoft.AspNet.Identity;
using HomeAutomation.Services.Interfaces;
using HomeAutomation.Controllers.Abstract;
using HomeAutomation.Services.Abstract;

namespace HomeAutomation.Controllers
{
    [Authorize]
    public class PermissionsController : BaseController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public PermissionsController(ApplicationDbContext context, IServiceHolder ss) : base(ss)
        {
            db = context;
        }

        // GET: Permissions
        public ActionResult Index(int? id)
        {
            var users = SS.PermissionService.GetAllUsersWithPermissionsForUserDevices();
            //var devicePermissions = db.DevicePermissions.Include(d => d.Device).Include(d => d.LocationKey);
            return View(users.ToList());
        }

        // GET: Permissions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DevicePermission devicePermission = db.DevicePermissions.Find(id);
            if (devicePermission == null)
            {
                return HttpNotFound();
            }
            return View(devicePermission);
        }

        // GET: Permissions/Create
        public ActionResult Create()
        {
            string currentUserId = User.Identity.GetUserId();
            ViewBag.DeviceId = new SelectList(db.Devices, "Id", "Name");
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "Name");
            ViewBag.UserId = new SelectList(db.Users.Where(u => u.Id != currentUserId), "Id", "UserName");

            return View();
        }

        // POST: Permissions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AddEditViewModel permission)
        {
            if (ModelState.IsValid)
            {
                if (permission.IsFullControlPermission)
                {
                    SS.PermissionService.AddLocationPermission(permission.UserId, permission.LocationId);
                }
                else
                {
                    SS.PermissionService.AddDevicePermission(permission.UserId, permission.LocationId, permission.DeviceId);
                }

                return RedirectToAction("Index");
            }

            string currentUserId = User.Identity.GetUserId();
            ViewBag.DeviceId = new SelectList(db.Devices, "Id", "Name");
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "Name");
            ViewBag.UserId = new SelectList(db.Users.Where(u => u.Id != currentUserId), "Id", "UserName");

            return View(permission);
        }

        // GET: Permissions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DevicePermission devicePermission = db.DevicePermissions.Find(id);
            if (devicePermission == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeviceId = new SelectList(db.Devices, "Id", "Name", devicePermission.DeviceId);
            ViewBag.LocationKeyId = new SelectList(db.LocationKeys, "Id", "Key", devicePermission.LocationKeyId);
            return View(devicePermission);
        }

        // POST: Permissions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,LocationKeyId,DeviceId,DateCreated,DateModified")] DevicePermission devicePermission)
        {
            if (ModelState.IsValid)
            {
                db.Entry(devicePermission).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DeviceId = new SelectList(db.Devices, "Id", "Name", devicePermission.DeviceId);
            ViewBag.LocationKeyId = new SelectList(db.LocationKeys, "Id", "Key", devicePermission.LocationKeyId);
            return View(devicePermission);
        }

        [HttpPost]
        public ActionResult RevokeForUser(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            SS.PermissionService.RevokeRightsForUser(id);

            return RedirectToAction("Index");
        }

        // GET: Permissions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DevicePermission devicePermission = db.DevicePermissions.Find(id);
            if (devicePermission == null)
            {
                return HttpNotFound();
            }
            return View(devicePermission);
        }

        // POST: Permissions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DevicePermission devicePermission = db.DevicePermissions.Find(id);
            db.DevicePermissions.Remove(devicePermission);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ForUser(string id)
        {
            var permissions = SS.PermissionService.GetAssignedPermissionsForUser(id);

            UserPermissionsViewModel vm = new UserPermissionsViewModel(permissions);

            return View(vm);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
