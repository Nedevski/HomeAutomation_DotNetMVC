﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeAutomation.DAL;
using HomeAutomation.Models.Entities;
using Microsoft.AspNet.Identity;
using HomeAutomation.Services.Interfaces;
using HomeAutomation.Services.Abstract;
using HomeAutomation.Controllers.Abstract;
using HomeAutomation.ViewModels.Devices;

namespace HomeAutomation.Controllers
{
    [Authorize]
    public class DevicesController : BaseController
    {
        public DevicesController(IServiceHolder ss) : base(ss)
        {
        }

        // GET: Devices
        public ActionResult Index()
        {
            var devices = SS.DeviceService.GetAllForUser();
            return View(devices.ToList());
        }

        // GET: Devices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = SS.DeviceService.GetSingleForUser(id.Value);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        // GET: Devices/Create
        public ActionResult Create()
        {
            var locationsForUser = SS.LocationService.GetAllForUser();

            ViewBag.LocationId = new SelectList(locationsForUser, "Id", "Name");

            return View();
        }

        // POST: Devices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AddEditDeviceViewModel deviceVM)
        {
            if (ModelState.IsValid)
            {
                var device = new Device()
                {
                    Name = deviceVM.Name,
                    Description = deviceVM.Description,
                    LocalIP = deviceVM.LocalIP,
                    Port = deviceVM.Port,
                    LocationId = deviceVM.LocationId,
                    Type = deviceVM.Type,
                    OwnerId = CurrentUserId,
                };

                SS.DeviceService.Add(device);

                return RedirectToAction("Index");
            }

            var locationsForUser = SS.LocationService.GetAllForUser();
            
            ViewBag.LocationId = new SelectList(locationsForUser, "Id", "Name", deviceVM.LocationId);
            return View(deviceVM);
        }

        // GET: Devices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = SS.DeviceService.GetSingleForUser(id.Value);
            if (device == null)
            {
                return HttpNotFound();
            }

            var locationsForUser = SS.LocationService.GetAllForUser();

            ViewBag.LocationId = new SelectList(locationsForUser, "Id", "Name", device.LocationId);
            return View(new AddEditDeviceViewModel(device));
        }

        // POST: Devices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AddEditDeviceViewModel device)
        {
            if (ModelState.IsValid)
            {
                var entry = SS.DeviceService.GetSingleForUser(device.Id);
                entry.Name = device.Name;
                entry.Description = device.Description;
                entry.LocalIP = device.LocalIP;
                entry.Port = device.Port;
                entry.Type = device.Type;

                SS.DeviceService.Update(entry);
                return RedirectToAction("Index");
            }

            return View(device);
        }

        // GET: Devices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = SS.DeviceService.GetSingleForUser(id.Value);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        // POST: Devices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SS.DeviceService.Delete(id);

            return RedirectToAction("Index");
        }
    }
}
