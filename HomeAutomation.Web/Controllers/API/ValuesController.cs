﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HomeAutomation.DAL;

namespace HomeAutomation.Web.Controllers.API
{
    public class ValuesController : ApiController
    {
        private ApplicationDbContext _context;
        
        public ValuesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET api/<controller>
        /// <summary>
        /// Gets some values
        /// </summary>
        /// <remarks>Gets a hardcoded array of string values</remarks>
        /// <returns>Array of values</returns>
        /// <response code="400">Bad request</response>
        /// <response code="404">NotFound</response>
        [HttpGet]
        [ResponseType(typeof(string[]))]
        public IHttpActionResult Get()
        {
            return Ok(new string[] { "value1", "value2" });
        }

        // GET api/<controller>/5
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            return Ok();
        }

        // POST api/<controller>
        [HttpPost]
        public IHttpActionResult Post([FromBody]string value)
        {
            return Ok();
        }

        // PUT api/<controller>/5
        [HttpPut]
        public IHttpActionResult Put(int id, [FromBody]string value)
        {
            return Ok();
        }

        // DELETE api/<controller>/5
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            return Ok();
        }
    }
}