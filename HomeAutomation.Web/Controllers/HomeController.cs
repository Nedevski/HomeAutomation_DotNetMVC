﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HomeAutomation.Controllers.Abstract;
using HomeAutomation.DAL;
using HomeAutomation.Services.Abstract;

namespace HomeAutomation.Web.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IServiceHolder ss) : base(ss)
        {
        }

        public ActionResult Index()
        {
			return RedirectToAction("Index", "Dashboard");
        }
    }
}