﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HomeAutomation.Services.Abstract;
using Microsoft.AspNet.Identity;

namespace HomeAutomation.Controllers.Abstract
{
    public abstract class BaseController : Controller
    {
        public BaseController(IServiceHolder ss)
        {
            this.SS = ss;
        }

        public IServiceHolder SS { get; private set; }

        protected string CurrentUserId
        {
            get
            {
                return User.Identity.GetUserId();
            }
        }

        protected string CurrentUserName
        {
            get
            {
                return User.Identity.GetUserName();
            }
        }
    }
}