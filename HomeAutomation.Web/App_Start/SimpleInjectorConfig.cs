﻿using System.Data.Entity;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using SimpleInjector.Integration.WebApi;
using HomeAutomation.DAL;
using HomeAutomation.DAL.Repositories;
using HomeAutomation.Models.Account;
using HomeAutomation.Models.Entities;
using HomeAutomation.Services.Interfaces;
using HomeAutomation.Services;
using HomeAutomation.Services.Abstract;
using HomeAutomation.Common;
//using HomeAutomation.Models.Entities;
//using HomeAutomation.Services;
//using HomeAutomation.Services.Interfaces;

namespace HomeAutomation.App_Start
{
    public class SimpleInjectorConfig
    {
        public static Container container;
        
        public static void RegisterDependencies()
        {
            container = new Container();
            
            var lifestyle = Lifestyle.CreateHybrid
            (
                lifestyleSelector: () => HttpContext.Current != null,
                trueLifestyle: new WebRequestLifestyle(),
                falseLifestyle: Lifestyle.Transient
            );

            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

			//// Register services
			container.Register<ILocationService, LocationService>(lifestyle);
			container.Register<ILocationKeyService, LocationKeyService>(lifestyle);
			container.Register<IDeviceService, DeviceService>(lifestyle);
			container.Register<IDashboardService, DashboardService>(lifestyle);
			container.Register<IPermissionService, PermissionService>(lifestyle);
			container.Register<IRemoteUserService, RemoteUserService>(lifestyle);

			container.Register<IRestService, RestService>(lifestyle);
			container.Register<IRemoteIntegrationService, RemoteIntegrationService>(lifestyle);

			container.Register<IServiceFactory, WebServiceFactory>(lifestyle);
            container.Register<IServiceHolder, ServiceHolder>(lifestyle);

            // Register repositories
            container.Register<IRepository<Location>, BaseRepository<Location>>(lifestyle);
			container.Register<IRepository<LocationKey>, BaseRepository<LocationKey>>(lifestyle);
			container.Register<IRepository<Device>, BaseRepository<Device>>(lifestyle);
			container.Register<IRepository<DevicePermission>, BaseRepository<DevicePermission>>(lifestyle);
			container.Register<IRepository<RemoteUser>, BaseRepository<RemoteUser>>(lifestyle);

			//Register common logic classes
			//container.RegisterSingleton<IHubContext>(() => GlobalHost.ConnectionManager.GetHubContext<GameHub>());

			// Registers DbContext to use the parameterless ApplicationDbContext constructor
			// and have a single DbContext per web request
			container.Register<ApplicationDbContext>(() => new ApplicationDbContext(), lifestyle);

            // Register controllers
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.RegisterMvcIntegratedFilterProvider();

            // Check if there are any errors with the dependency registrations
            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}
