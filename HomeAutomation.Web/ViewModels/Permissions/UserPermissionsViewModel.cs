﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HomeAutomation.Models.Entities;

namespace HomeAutomation.ViewModels.Permissions
{
    public class UserPermissionsViewModel
    {
        public UserPermissionsViewModel(Dictionary<Location, IEnumerable<Device>> permissions)
        {
            LocationPermissions = new List<LocationPermissionViewModel>();

            if (permissions.Count > 0)
            {
                foreach (var kvp in permissions)
                {
                    LocationPermissionViewModel temp = new LocationPermissionViewModel();
                    temp.Location = kvp.Key;

                    if (kvp.Value == null)
                    {
                        temp.HasFullPermissions = true;
                    }
                    else
                    {
                        temp.HasFullPermissions = false;
                        temp.AllowedDevices = kvp.Value.ToList();
                    }

                    LocationPermissions.Add(temp);
                }
            }
        }

        public List<LocationPermissionViewModel> LocationPermissions { get; set; }
    }
}