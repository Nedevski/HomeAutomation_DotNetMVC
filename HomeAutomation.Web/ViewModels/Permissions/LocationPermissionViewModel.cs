﻿using System.Collections.Generic;
using HomeAutomation.Models.Entities;

namespace HomeAutomation.ViewModels.Permissions
{
    public class LocationPermissionViewModel
    {
        public Location Location { get; set; }
        public bool HasFullPermissions { get; set; }
        public List<Device> AllowedDevices { get; set; }
    }
}