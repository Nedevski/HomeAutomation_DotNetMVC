﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeAutomation.ViewModels.Permissions
{
    public class AddEditViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int LocationId { get; set; }
        public int DeviceId { get; set; }
        public bool IsFullControlPermission { get; set; }
    }
}