﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using HomeAutomation.Models.Entities;
using HomeAutomation.Models.Enumeration;

namespace HomeAutomation.ViewModels.Devices
{
    public class AddEditDeviceViewModel
    {
        public AddEditDeviceViewModel()
        {

        }

        public AddEditDeviceViewModel(Device device)
        {
            Id = device.Id;
            Name = device.Name;
            Description = device.Description;
            LocalIP = device.LocalIP;
            Port = device.Port;
            Type = device.Type;
            OwnerId = device.OwnerId;
            LocationId = device.LocationId;
        }

        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public string LocalIP { get; set; }

        public int Port { get; set; }

        [Required]
        public DeviceType Type { get; set; }

        public string OwnerId { get; set; }

        [Required]
        public int LocationId { get; set; }
    }
}