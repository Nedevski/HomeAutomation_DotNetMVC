﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using HomeAutomation.Models.Account;
using HomeAutomation.Models.Entities;

namespace HomeAutomation.ViewModels.Locations
{
	public class AddEditLocationViewModel
	{
		public AddEditLocationViewModel()
		{

		}

		public AddEditLocationViewModel(Location location)
		{
			Id = location.Id;
			Name = location.Name;
			Description = location.Description;
			LocationURL = location.LocationURL;
			Port = location.Port;
            MasterKey = location.MasterKey;
            OwnerId = location.OwnerId;
		}

		public int Id { get; set; }

		[Required]
		public string Name { get; set; }

		public string Description { get; set; }

		[Required]
		public string LocationURL { get; set; }

		public int Port { get; set; }
        
		[Required]
		public string MasterKey { get; set; }

        public string OwnerId { get; set; }
	}
}