﻿"use strict"
$(function () {
    const SENSOR_CLASS = ".deviceInit_Sensor";
    const SWITCH_CLASS = ".deviceInit_Switch";
    const SLIDER_CLASS = ".deviceInit_Slider";
    const TEXTSENDER_CLASS = ".deviceInit_TextSender";

    function initAllDevices() {
        initSensors();
        initSwitches();
        //initSliders();
        //initTextSenders();
    };

    function initSensors() {
        var $sensorDivs = $(SENSOR_CLASS);

        $sensorDivs.each(function (index, device) {
            var $device = $(device);

            var deviceId = $device.data("id");
            var locationId = $device.parent().data("locationId");
            var localUrl = "http://" + $device.parent().data("localAddress") + "/status/p1";

            var statusRequest = {
                locationId: locationId,
                url: localUrl
            };

            var timer = setInterval(function () {
                var isCheckingStatus = $device.data("isCheckingStatus");

                if (!isCheckingStatus) {
                    $device.data("isCheckingStatus", 1);

                    DASH.sendRequest(statusRequest,
                    function (data) {
                        $device.find(".deviceStatus").text(data.value);
                    },
                    function (err) {
                        if (err.responseJSON != undefined) {
                            console.log(err);
                        }
                    },
                    function () {
                        $device.data("isCheckingStatus", 0);
                    }
                  );
                }
            }, 1000);
        });
    }

    function initSwitches() {
        var $switchDivs = $(SWITCH_CLASS);

        // init devices one at a time
        $switchDivs.each(function (index, device) {
            var $device = $(device);
            SWITCH_CTRL.changeDisabledStatus($device, true);

            var deviceId = $device.data("id");
            var locationId = $device.parent().data("locationId");
            var statusUrl = "http://" + $device.parent().data("localAddress") + "/status/p1";
            var turnOnUrl = "http://" + $device.parent().data("localAddress") + "/p1/1";
            var turnOffUrl = "http://" + $device.parent().data("localAddress") + "/p1/0";

            var statusRequest = {
                locationId: locationId,
                url: statusUrl
            };

            var turnOnRequest = {
                locationId: locationId,
                url: turnOnUrl
            };

            var turnOffRequest = {
                locationId: locationId,
                url: turnOffUrl
            };

            // ensure status checks every X ms if there isn't an active request for this device
            var timer = setInterval(function () {
                var isCheckingStatus = $device.data("isCheckingStatus");
                var isRunningUserCommand = $device.data("isRunningUserCommand");

                if (!isCheckingStatus && !isRunningUserCommand) {
                    $device.data("isCheckingStatus", 1);

                    DASH.sendRequest(statusRequest,
                      function (data) {
                          SWITCH_CTRL.updateSwitchStateFromStatusCheck($device, data.value);
                          SWITCH_CTRL.changeDisabledStatusFromStatusCheck($device, false); // reenable the buttons
                          $device.find(".deviceStatus").text(data.value);
                      },
                      function (err) {
                          if (err.responseJSON != undefined) {
                              console.log(err);
                          }
                      },
                      function (data) {
                          $device.data("isCheckingStatus", 0);
                          $device.find(".deviceStatus").text(data.value);
                      }
                    );
                }
            }, 1000);

            // attach onClick handlers
            $device.on("click", ".switch-control", function () {
                var isRunningUserCommand = $device.data("isRunningUserCommand");
                var isDisabled = $device.prop('disabled');

                if (!isRunningUserCommand && !isDisabled) {
                    var userCommand = $(this).data("val"); // the value the user wants to set (0/1)

                    $device.data("isRunningUserCommand", 1); // set flag that user command is in progress
                    SWITCH_CTRL.updateSwitchState($device, userCommand); // update switch visual according to the button pressed
                    SWITCH_CTRL.changeDisabledStatus($device, true); // disable the buttons

                    var requestToSend = userCommand == 1 ? turnOnRequest : turnOffRequest;

                    DASH.sendRequest(requestToSend,
                      function (data) {
                          SWITCH_CTRL.updateSwitchState($device, data.value);
                          SWITCH_CTRL.changeDisabledStatus($device, false); // reenable the buttons
                          console.log(data);
                      },
                      function (err) {
                          if (err.responseJSON != undefined) {
                              console.log(err);
                          }
                      },
                      function () {
                          $device.data("isRunningUserCommand", 0); // set flag that user command has finished
                      }
                    );
                }
            });
        });
    }

    initAllDevices();
});