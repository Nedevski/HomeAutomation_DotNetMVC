﻿"use strict"
$(function () {
    var switchController = function () {
        var changeDisabledStatusFromStatusCheck = function ($control, state) {
            if (!$control.data("isRunningUserCommand")) {
                changeDisabledStatus($control, state);
            }
        }

        // enables/disables the switch
        var changeDisabledStatus = function ($control, state) {
            var $container = $control.find(".switch-control-container");

            if (state) {
                $container.children(".switch-control").addClass('disabled');
            }
            else {
                $container.children(".switch-control").removeClass('disabled');
            }

            $control.prop('disabled', state);
        }

        var updateSwitchStateFromStatusCheck = function($device, state) {
            if (!$device.data("isRunningUserCommand")) {
                updateSwitchState($device, state);
            }
        }

        // switches buttons between on and off state
        var updateSwitchState = function ($device, state) {
            $device.data("status", state);
            var $container = $device.find(".switch-control-container");

            if (state == 1) {
                $container.children(".switch-on").removeClass("btn-default").addClass("btn-success").addClass("active");
                $container.children(".switch-off").removeClass("btn-danger").removeClass("active").addClass("btn-default");
            }
            else {
                $container.children(".switch-on").removeClass("btn-success").removeClass("active").addClass("btn-default");
                $container.children(".switch-off").removeClass("btn-default").addClass("btn-danger").addClass("active");
            }
        }

        return {
            changeDisabledStatus: changeDisabledStatus,
            changeDisabledStatusFromStatusCheck: changeDisabledStatusFromStatusCheck,
            updateSwitchState: updateSwitchState,
            updateSwitchStateFromStatusCheck: updateSwitchStateFromStatusCheck,
        };
    }();

    window.SWITCH_CTRL = switchController;
});