﻿"use strict"
$(function () {
  // helper global object for creating new requests
  // global name = REQ
  var requestTemplate = function () {
    var createRequestTemplate = function (locationId, url, data) {
      return {
        locationId: locationId,
        url: url,
        data: data
      };
    };

    return {
      create: createRequestTemplate
    };    
  };

  // core dashboard object, all requests are sent through here
  // global name = DASH
  var dashCore = function () {
    var keysHolder = [];

    // gets all location keys and stores them in keysHolder variable
    // result is array of objects with properties LocationId and LocationKey
    var getAllKeysForUser = function () {
      var $userInfo = $("#user-info");
      var userId = $userInfo.data("userId");
      var token = $userInfo.find("input[name='__RequestVerificationToken']").val();

      $.ajax({
        url: "/Dashboard/GetLocationKeysForUser",
        data: {
          __RequestVerificationToken: token,
          userId: userId
        },
        dataType: 'json',
        method: "post",
        success: function (data) {
          keysHolder = data;
        },
        error: function (err) {
          console.log(err);
        }
      });
    };

    var getKeyForCompany = function (locationId) {
      var key;

      keysHolder.forEach(function (keyObj) {
        if (keyObj.LocationId == locationId) {
          key = keyObj;
        }
      });

      return key;
    };

    var sendRequest = function (request, onSuccess, onError, onComplete) {
      var keyObject = getKeyForCompany(request.locationId); // get a valid company key or false

      if (keyObject) {
        //keyObject.FullUrl = "https://www.random.org/integers/?num=1&col=1&min=1&max=1000&format=plain&base=10&rnd=new";

        try {
          $.ajax({
            url: keyObject.FullUrl + "/execute.php",
            data: {
              authKey: keyObject.LocationKey, // attach location key to request
              url: request.url
            },
            dataType: "json",
            method: "post",
            success: onSuccess,
            error: function (err) {
              if (err.responseJSON != undefined) {
                console.log(err);
                onError(err);
              }
            },
            complete: onComplete
          });
        }
        catch (error) {
          console.log(error);
        }
      }
    };

    var init = function () {
      getAllKeysForUser();
    };

    return {
      sendRequest: sendRequest,
      init: init
    };
  }();

  window.DASH = dashCore;
  window.REQ = requestTemplate;

  DASH.init();
});