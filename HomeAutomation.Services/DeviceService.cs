﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeAutomation.DAL.Repositories;
using HomeAutomation.Models.Entities;
using HomeAutomation.Services.Abstract;
using HomeAutomation.Services.Interfaces;

namespace HomeAutomation.Services
{
	public class DeviceService : EntityServiceBase<Device, IRepository<Device>>, IDeviceService
	{
        public DeviceService(IServiceHolder ss, IRepository<Device> repo) : base(ss, repo)
		{
        }
        
        public IEnumerable<Device> GetAllForUser()
        {
            //return base.GetAll()
            //    .Where(
            //        d => d.Location.LocationKeys.Any(k => k.OwnerId == CurrentUserId && k.IsMasterKey) || 
            //        d.Permissions.Any(p => p.LocationKey.OwnerId == CurrentUserId && p.DeviceId == d.Id)
            //    ); 

            string currUserId = CurrentUserId;

            return base.GetAll()
                .Where(d => d.Location.OwnerId == currUserId);
        }

        public Device GetSingleForUser(int id)
        {
            if (!CurrentUserIsDeviceOwner(id))
            {
                return null;
            }

            return base.GetSingle(id);
        }

        public override bool Delete(int id, bool withSave = true)
		{
            Device deviceToDelete = GetAll().Where(d => d.Id == id).SingleOrDefault();

            if (deviceToDelete == null)
            {
                return false;
            }

            return this.Delete(deviceToDelete, withSave);
		}

        public override bool Delete(Device deviceToDelete, bool withSave = true)
        {
            foreach (var devicePermission in deviceToDelete.Permissions.ToList())
            {
                SS.PermissionService.RevokeDevicePermission(devicePermission, false);
            }

            base.Delete(deviceToDelete, withSave);

            return true;
        }

        public override bool Delete(ICollection<Device> devices, bool withSave = true)
        {
            foreach (var d in devices)
            {
                this.Delete(d, false);
            }

            if (withSave)
            {
                SaveChanges();
            }

            return true;
        }

        public bool CurrentUserIsDeviceOwner(int deviceId)
        {
            return GetAll().Any(d => d.OwnerId == CurrentUserId);
        }
    }
}
