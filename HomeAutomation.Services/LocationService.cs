﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Web;
using HomeAutomation.DAL.Repositories;
using HomeAutomation.Models.Entities;
using HomeAutomation.Services.Interfaces;
using HomeAutomation.Services.Abstract;

namespace HomeAutomation.Services
{
	public class LocationService : EntityServiceBase<Location, IRepository<Location>>, ILocationService
	{
        public LocationService(IServiceHolder ss, IRepository<Location> repo) : base(ss, repo)
		{
		}
		
		public IQueryable<Location> GetAllForUser()
		{
            return base.GetAll()
                .Where(loc => loc.OwnerId == CurrentUserId);
        }        

        public Location GetSingleForUser(int id)
		{
			if (!CurrentUserIsLocationOwner(id))
			{
				return null;
			}

			return base.GetSingle(id);
		}

        public override bool Add(Location location, bool withSave = true)
        {
            bool success = base.Add(location, withSave);

            if (!location.IsVerified)
            {
                location.IsVerified = SS.RemoteIntegrationService.VerifyLocation(location);
            }

            return success;
        }

        public override bool Update(Location location, bool withSave = true)
        {
            if (!location.IsVerified)
            {
                location.IsVerified = SS.RemoteIntegrationService.VerifyLocation(location);
            }

            return base.Update(location, withSave);
        }

        public override bool Delete(int id, bool withSave = true)
		{
			Location locationToDelete = GetAll().Where(l => l.Id == id).SingleOrDefault();

			if (locationToDelete == null)
			{
				return false;
			}

			foreach (var key in locationToDelete.LocationKeys.ToList())
			{
				SS.LocationKeyService.Delete(key, false);
			}

            foreach (var device in locationToDelete.Devices.ToList())
            {
                foreach (var devicePermission in device.Permissions.ToList())
                {
                    SS.PermissionService.RevokeDevicePermission(devicePermission, false);
                }

                SS.DeviceService.Delete(device, false);
            }

            foreach (var user in locationToDelete.RemoteUsers.ToList())
            {
                SS.RemoteUserService.Delete(user, false);
            }

            return base.Delete(locationToDelete, withSave);
		}

        public bool CurrentUserIsLocationOwner(int locationId)
        {
            return GetAll().Any(loc => loc.Id == locationId && loc.OwnerId == CurrentUserId);
        }

        public bool VerifyLocation(Location location)
        {
            return SS.RemoteIntegrationService.VerifyLocation(location);
        }
    }
}
