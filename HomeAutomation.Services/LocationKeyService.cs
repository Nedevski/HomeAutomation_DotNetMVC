﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeAutomation.DAL.Repositories;
using HomeAutomation.Models.Entities;
using HomeAutomation.Models.Wrappers;
using HomeAutomation.Services.Abstract;
using HomeAutomation.Services.Interfaces;

namespace HomeAutomation.Services
{
	public class LocationKeyService : EntityServiceBase<LocationKey, IRepository<LocationKey>>, ILocationKeyService
	{
		public LocationKeyService(IServiceHolder ss, IRepository<LocationKey> keyRepo) : base(ss, keyRepo)
		{
		}

		public List<LocationKeyWrapper> GetLocationKeysForUser()
		{
            IQueryable<Location> ownedLocations = SS.LocationService
                .GetAll()
                .Where(k => k.OwnerId == CurrentUserId);

            IQueryable<Location> sharedLocations = SS.LocationKeyService
                .GetAll()
                .Where(lk => lk.OwnerId == CurrentUserId)
                .Select(lk => lk.Location);

            List<LocationKeyWrapper> allLocationKeys = ownedLocations
                .Union(sharedLocations)
				.Select(k => new LocationKeyWrapper() {
					LocationId = k.Id,
					LocationKey = k.MasterKey,
					URL = k.LocationURL,
					Port = k.Port
				})
				.ToList();

            return allLocationKeys;
		}
	}
}
