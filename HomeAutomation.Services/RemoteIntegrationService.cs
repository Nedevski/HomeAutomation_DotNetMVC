﻿using System.Linq;
using System.Net;
using System.Web;
using HomeAutomation.DAL;
using HomeAutomation.DAL.Repositories;
using HomeAutomation.Models.Account;
using HomeAutomation.Models.Entities;
using HomeAutomation.Models.IntegrationProxies;
using Microsoft.AspNet.Identity;
using RestSharp;
using HomeAutomation.Services.Interfaces;
using HomeAutomation.Services.Abstract;
using System;

namespace HomeAutomation.Services
{
    public class RemoteIntegrationService : EntityServiceBase, IRemoteIntegrationService
    {
        private ApplicationDbContext _context;

        private const string USER_CREATE_URL = "/api/user/create.php";
        private const string LOCATION_VERIFY = "/api/location/verify.php";

        public RemoteIntegrationService(IServiceHolder ss, ApplicationDbContext context) : base(ss)
        {
            _context = context;
        }

        public bool VerifyLocation(Location location)
        {
            // only allow user to verify a location if he owns it or he is granted permission to it
            if (location == null || location.IsVerified)
            {
                return false;
            }

            object requestObj = new
            {
                userId = location.OwnerId,
            };

            IRestRequest restRequest = SS.RestService.CreateJsonRequest(Method.POST, LOCATION_VERIFY, location.MasterKey, requestObj);
            IRestResponse<VerifyLocationMessageResponse> response = SS.RestService.Execute<VerifyLocationMessageResponse>(restRequest, location.FullUrl);

            if (response.StatusCode != HttpStatusCode.OK ||
                response.Data == null ||
                !response.Data.UserGuid.HasValue)
            {
                return false;
            }

            RemoteUser existingRemoteUser = SS.RemoteUserService
                .GetAll()
                .SingleOrDefault(ru => ru.LocalUserId == location.OwnerId && ru.LocationId == location.Id);

            if (existingRemoteUser == null)
            {
                RemoteUser newRemoteUser = new RemoteUser()
                {
                    LocalUserId = location.OwnerId,
                    RemoteUserGuid = response.Data.UserGuid.Value,
                    LocationId = location.Id,
                };

                SS.RemoteUserService.Add(newRemoteUser, false);
            }

            if (!location.IsVerified)
            {
                location.IsVerified = true;
                SS.LocationService.Update(location);
            }

            return true;
        }

        public RemoteUser RegisterNewRemoteUser(string userId, int locationId, out string authKey)
        {
            authKey = string.Empty;
            string currentUserId = CurrentUserId;

            Location location = SS.LocationService.GetAll()
                .SingleOrDefault(l => l.Id == locationId && l.OwnerId == currentUserId);

            if (location == null)
            {
                return null;
            }

            ApplicationUser userToRegister = _context.Users.SingleOrDefault(u => u.Id == userId);

            if (userToRegister == null)
            {
                return null;
            }

            object requestObj = new
            {
                userId = userToRegister.Id,
                username = userToRegister.UserName
            };

            IRestRequest restRequest = SS.RestService.CreateJsonRequest(Method.POST, USER_CREATE_URL, location.MasterKey, requestObj);
            IRestResponse<RegisterNewUserMessageResponse> response = SS.RestService.Execute<RegisterNewUserMessageResponse>(restRequest, location.FullUrl);

            if (response.StatusCode != HttpStatusCode.Created ||
                response.Data == null ||
                !response.Data.UserGuid.HasValue ||
                string.IsNullOrEmpty(response.Data.UserKey))
            {
                return null;
            }

            RemoteUser remoteUser = new RemoteUser()
            {
                LocalUserId = currentUserId,
                RemoteUserGuid = response.Data.UserGuid.Value,
                LocationId = locationId,
            };

            authKey = response.Data.UserKey;
            return remoteUser;
        }
    }
}
