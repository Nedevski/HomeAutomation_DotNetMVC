﻿using System.Web;
using Microsoft.AspNet.Identity;

namespace HomeAutomation.Services.Abstract
{
    public class EntityServiceBase
    {
        public EntityServiceBase(IServiceHolder SS)
        {
            this.SS = SS;
        }

        public virtual IServiceHolder SS { get; set; }

        public string CurrentUserId
        {
            get
            {
                return HttpContext.Current.User.Identity.GetUserId();
            }
        }
    }
}
