﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeAutomation.Services.Interfaces;

namespace HomeAutomation.Services.Abstract
{
    public class ServiceHolder : IServiceHolder
    {
        private IServiceFactory factory;

        public ServiceHolder(IServiceFactory factory)
        {
            this.factory = factory;
        }

        public IDashboardService DashboardService { get { return factory.GetService<IDashboardService>(); } }
        public IDeviceService DeviceService { get { return factory.GetService<IDeviceService>(); } }
        public ILocationService LocationService { get { return factory.GetService<ILocationService>(); } }
        public ILocationKeyService LocationKeyService { get { return factory.GetService<ILocationKeyService>(); } }
        public IPermissionService PermissionService { get { return factory.GetService<IPermissionService>(); } }
        public IRemoteUserService RemoteUserService { get { return factory.GetService<IRemoteUserService>(); } }
        public IRemoteIntegrationService RemoteIntegrationService { get { return factory.GetService<IRemoteIntegrationService>(); } }
        public IRestService RestService { get { return factory.GetService<IRestService>(); } }

    }
}
