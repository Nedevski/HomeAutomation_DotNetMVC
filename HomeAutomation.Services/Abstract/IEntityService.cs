﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeAutomation.Models;

namespace HomeAutomation.Services.Abstract
{
    public interface IEntityService<T> where T : BaseEntity
    {
        IQueryable<T> GetAll();

        T GetSingle(int id);

        Task<T> GetSingleAsync(int id);

        bool Add(T entity, bool withSave = true);
        bool Add(ICollection<T> entities, bool withSave = true);

        bool Update(T entity, bool withSave = true);
        bool Update(ICollection<T> entities, bool withSave = true);

        bool Delete(T entity, bool withSave = true);
        bool Delete(int id, bool withSave = true);
        bool Delete(ICollection<T> entities, bool withSave = true);

        int? SaveChanges();

        Task<int?> SaveChangesAsync();
    }
}
