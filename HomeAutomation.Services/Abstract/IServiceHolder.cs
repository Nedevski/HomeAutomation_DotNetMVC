﻿using HomeAutomation.Services.Interfaces;

namespace HomeAutomation.Services.Abstract
{
    public interface IServiceHolder
    {
        IDashboardService DashboardService { get; }
        IDeviceService DeviceService { get; }
        ILocationKeyService LocationKeyService { get; }
        ILocationService LocationService { get; }
        IPermissionService PermissionService { get; }
        IRemoteUserService RemoteUserService { get; }
        IRemoteIntegrationService RemoteIntegrationService { get; }
        IRestService RestService { get; }
    }
}