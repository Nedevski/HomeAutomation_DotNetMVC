﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeAutomation.DAL.Repositories;
using HomeAutomation.Models;

namespace HomeAutomation.Services.Abstract
{
    public class EntityServiceBase<E, R> : EntityServiceBase, IEntityService<E>
        where E : BaseEntity
        where R : IRepository<E>
    {
        private R repository;

        public EntityServiceBase(IServiceHolder serviceHolder, R repository) : base(serviceHolder)
        {
            this.repository = repository;
        }
        
        public virtual IQueryable<E> GetAll()
        {
            return repository.GetAll();
        }

        public virtual E GetSingle(int id)
        {
            return GetAll().FirstOrDefault(e => e.Id == id);
        }

        public virtual bool Add(E entity, bool withSave = true)
        {
            SetCreatedFields(entity);

            try
            {
                repository.Add(entity, withSave);
            }
            catch (Exception ex)
            {
                LogException(ex);
                return false;
            }

            return true;
        }

        public virtual bool Add(ICollection<E> entities, bool withSave = true)
        {
            foreach (var entity in entities)
            {
                this.SetCreatedFields(entity);
            }

            try
            {
                repository.Add(entities, withSave);
            }
            catch (Exception ex)
            {
                LogException(ex);
                return false;
            }

            return true;
        }

        public virtual bool Update(E entity, bool withSave = true)
        {
            SetUpdatedFields(entity);

            try
            {
                repository.Update(entity, withSave);
            }
            catch (Exception ex)
            {
                LogException(ex);

                return false;
            }

            return true;
        }
        
        public virtual bool Update(ICollection<E> entities, bool withSave = true)
        {
            foreach (E entity in entities)
            {
                this.SetUpdatedFields(entity);
            }

            try
            {
                repository.Update(entities, withSave);
            }
            catch (Exception ex)
            {
                LogException(ex);
                return false;
            }

            return true;
        }

        public virtual bool Delete(int id, bool withSave = true)
        {
            try
            {
                repository.PermaDelete(id, withSave);
                return true;
            }
            catch (Exception ex)
            {
                LogException(ex);
                return false;
            }
        }
        
        public virtual bool Delete(E entity, bool withSave = true)
        {
            try
            {
                repository.PermaDelete(entity, withSave);
                return true;
            }
            catch (Exception ex)
            {
                LogException(ex);
                return false;
            }
        }

        public virtual bool Delete(ICollection<E> entities, bool withSave = true)
        {
            try
            {
                repository.PermaDelete(entities, withSave);

                return true;
            }
            catch (Exception ex)
            {
                LogException(ex);

                return false;
            }
        }

        public Task<E> GetSingleAsync(int id)
        {
            throw new NotImplementedException();
        }

        public int? SaveChanges()
        {
            return repository.SaveChanges();
        }

        public Task<int?> SaveChangesAsync()
        {
            throw new NotImplementedException();
        }

        private void SetCreatedFields(E entity)
        {
            entity.DateCreated = DateTime.UtcNow;
            entity.DateModified = default(DateTime?);
        }

        private void SetUpdatedFields(E entity)
        {
            entity.DateModified = DateTime.UtcNow;
        }

        private void LogException(Exception up)
        {
            throw up;
        }
    }
}
