﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeAutomation.DAL.Repositories;
using HomeAutomation.Models.Entities;
using HomeAutomation.Services.Abstract;
using HomeAutomation.Services.Interfaces;

namespace HomeAutomation.Services
{
    public class RemoteUserService : EntityServiceBase<RemoteUser, IRepository<RemoteUser>>, IRemoteUserService
    {
        public RemoteUserService(IServiceHolder ss, IRepository<RemoteUser> repo) : base(ss, repo)
        {
        }
    }
}
