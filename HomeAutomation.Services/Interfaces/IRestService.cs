﻿using System.Threading.Tasks;
using RestSharp;

namespace HomeAutomation.Services.Interfaces
{
    public interface IRestService
    {
        IRestRequest CreateJsonRequest(Method verb, string resource, string key, object body = null);
        IRestResponse<T> Execute<T>(IRestRequest request, string baseUrl) where T : new();
        Task<IRestResponse<T>> ExecuteAsync<T>(IRestRequest request, string baseUrl) where T : new();
    }
}