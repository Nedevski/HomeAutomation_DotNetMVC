﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeAutomation.Models.Account;
using HomeAutomation.Models.Entities;

namespace HomeAutomation.Services.Interfaces
{
    public interface IPermissionService
    {
        IEnumerable<ApplicationUser> GetAllUsersWithPermissionsForUserDevices();
        Dictionary<Location, IEnumerable<Device>> GetAssignedPermissionsForUser(string assignedUserId);
        void AddLocationPermission(string userId, int locationId);
        void AddDevicePermission(string userId, int locationId, int deviceId);
        void RevokeRightsForUser(string id);
        void RevokeDevicePermission(DevicePermission devicePermission, bool withSave = true);
        void RevokeLocationKey(LocationKey locationKey, bool withSave = true);
    }
}
