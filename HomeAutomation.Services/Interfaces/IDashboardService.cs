﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeAutomation.Models.Wrappers;

namespace HomeAutomation.Services.Interfaces
{
	public interface IDashboardService
	{
		DashboardIndexViewModel PopulateViewModelForUser(string userId);
	}
}
