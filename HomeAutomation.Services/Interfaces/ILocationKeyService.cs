﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeAutomation.Models.Entities;
using HomeAutomation.Models.Wrappers;
using HomeAutomation.Services.Abstract;

namespace HomeAutomation.Services.Interfaces
{
	public interface ILocationKeyService : IEntityService<LocationKey>
	{
		List<LocationKeyWrapper> GetLocationKeysForUser();
	}
}
