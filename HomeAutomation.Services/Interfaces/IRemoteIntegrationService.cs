﻿using HomeAutomation.Models.Entities;

namespace HomeAutomation.Services.Interfaces
{
    public interface IRemoteIntegrationService
    {
        string CurrentUserId { get; }

        bool VerifyLocation(Location location);

        RemoteUser RegisterNewRemoteUser(string userId, int locationId, out string userKey);
    }
}