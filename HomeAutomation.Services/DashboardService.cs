﻿using System;
using System.Collections.Generic;
using System.Linq;
using HomeAutomation.DAL.Repositories;
using HomeAutomation.Models.Entities;
using HomeAutomation.Models.Wrappers;
using HomeAutomation.Services.Abstract;
using HomeAutomation.Services.Interfaces;

namespace HomeAutomation.Services
{
    public class DashboardService : EntityServiceBase, IDashboardService
    {
        public DashboardService(IServiceHolder ss) : base(ss)
        {
        }

        public DashboardIndexViewModel PopulateViewModelForUser(string userId)
        {
            DashboardIndexViewModel vm = new DashboardIndexViewModel();

            PopulateLocationsAndDevicesForUser(vm, userId);
            PopulateStatisticsForUser(vm, userId);

            return vm;
        }

        private void PopulateLocationsAndDevicesForUser(DashboardIndexViewModel vm, string userId)
        {
            var allUserLocationKeysForUser = SS.LocationKeyService.GetAll().Where(k => k.OwnerId == userId);

            List<UserLocationWrapper> userOwnedLocations = SS.LocationService
                .GetAll()
                .Where(loc => loc.OwnerId == userId)
                .Select(loc => new UserLocationWrapper()
                {
                    Id = loc.Id,
                    Name = loc.Name,
                    Description = loc.Description,
                    LocationURL = loc.LocationURL,
                    Port = loc.Port,
                    LocationKey = loc.MasterKey,
                    UserDevices = loc.Devices.Select(d => new UserDeviceWrapper()
                    {
                        Id = d.Id,
                        Name = d.Name,
                        Description = d.Description,
                        LocalIP = d.LocalIP,
                        Port = d.Port,
                        Type = d.Type,
                        LocationId = d.LocationId
                    })
                    .ToList()
                })
                .ToList();

            List<UserLocationWrapper> userSharedLocationsWithFullControl = SS.LocationKeyService
                .GetAll()
                .Where(lk => lk.OwnerId == userId && lk.HasFullDeviceControl)
                .Select(lk => new UserLocationWrapper()
                {
                    Id = lk.Location.Id,
                    Name = lk.Location.Name,
                    Description = lk.Location.Description,
                    LocationURL = lk.Location.LocationURL,
                    Port = lk.Location.Port,
                    LocationKey = lk.Location.MasterKey,
                    UserDevices = lk.Location.Devices
                        .Select(d => new UserDeviceWrapper()
                        {
                            Id = d.Id,
                            Name = d.Name,
                            Description = d.Description,
                            LocalIP = d.LocalIP,
                            Port = d.Port,
                            Type = d.Type,
                            LocationId = d.LocationId
                        }).ToList()
                })
                .ToList();

            List<UserLocationWrapper> userSharedLocationsWithoutFullControl = SS.LocationKeyService
                .GetAll()
                .Where(lk => lk.OwnerId == userId && !lk.HasFullDeviceControl)
                .Select(lk => new UserLocationWrapper()
                {
                    Id = lk.Location.Id,
                    Name = lk.Location.Name,
                    Description = lk.Location.Description,
                    LocationURL = lk.Location.LocationURL,
                    Port = lk.Location.Port,
                    LocationKey = lk.Location.MasterKey,
                    UserDevices = lk.Permissions
                        .Select(p => new UserDeviceWrapper()
                        {
                            Id = p.Device.Id,
                            Name = p.Device.Name,
                            Description = p.Device.Description,
                            LocalIP = p.Device.LocalIP,
                            Port = p.Device.Port,
                            Type = p.Device.Type,
                            LocationId = p.Device.LocationId
                        }).ToList()
                })
                .ToList();

            List<UserLocationWrapper> allLocations = new List<UserLocationWrapper>();

            allLocations.AddRange(userOwnedLocations);
            allLocations.AddRange(userSharedLocationsWithFullControl);
            allLocations.AddRange(userSharedLocationsWithoutFullControl);

            vm.UserLocations = allLocations;
        }

        private void PopulateStatisticsForUser(DashboardIndexViewModel vm, string userId)
        {
            IQueryable<Location> allUserLocations = SS.LocationService.GetAll().Where(loc => loc.OwnerId == userId);

            vm.LocationCount = allUserLocations.Count();
            vm.DevicesCount = allUserLocations.SelectMany(c => c.Devices).Count();

            vm.LocationsSharedFromOtherUsersCount = SS.LocationKeyService.GetAll().Where(k => k.OwnerId == userId).Count();
        }
    }
}
