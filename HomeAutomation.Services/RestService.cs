﻿using System.Threading.Tasks;
using RestSharp;
using HomeAutomation.Services.Interfaces;

namespace HomeAutomation.Services
{
    public class RestService : IRestService
    {
        private const int DEFAULT_TIMEOUT = 30000;

        public IRestRequest CreateJsonRequest(Method verb, string resource, string key, object body = null)
        {
            var request = new RestRequest(verb)
            {
                RequestFormat = DataFormat.Json,
                Resource = resource
            };

            if (!string.IsNullOrWhiteSpace(key))
            {
                request.AddHeader("Key", key);
            }

            request.AddHeader("Content-Type", "application/json;charset=utf-8");

            if (!ReferenceEquals(null, body))
            {
                request.AddBody(body);
            }

            return request;
        }

        public IRestResponse<T> Execute<T>(IRestRequest request, string baseUrl) where T : new()
        {
            return GetNewRestClient(baseUrl).Execute<T>(request);
        }

        public Task<IRestResponse<T>> ExecuteAsync<T>(IRestRequest request, string baseUrl) where T : new()
        {
            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();

            GetNewRestClient(baseUrl).ExecuteAsync<T>(request, (response) =>
            {
                taskCompletionSource.SetResult(response);
            });

            return taskCompletionSource.Task;
        }

        private static RestClient GetNewRestClient(string baseUrl)
        {
            return new RestClient(baseUrl)
            {
                Timeout = DEFAULT_TIMEOUT
            };
        }
    }
}
