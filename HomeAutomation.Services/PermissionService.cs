﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeAutomation.DAL.Repositories;
using HomeAutomation.Models.Account;
using HomeAutomation.Models.Entities;
using HomeAutomation.Services.Abstract;
using HomeAutomation.Services.Interfaces;

namespace HomeAutomation.Services
{
    public class PermissionService : EntityServiceBase<DevicePermission, IRepository<DevicePermission>>, IPermissionService
    {
        public PermissionService(IServiceHolder ss, IRepository<DevicePermission> repo) : base(ss, repo)
        {
        }

        public IEnumerable<ApplicationUser> GetAllUsersWithPermissionsForUserDevices()
        {
            return SS.LocationService.GetAll()
                .Where(l => l.OwnerId == CurrentUserId)
                .SelectMany(l => l.LocationKeys
                    .Where(lk => lk.OwnerId != CurrentUserId)
                    .Select(lk => lk.Owner))
                .Distinct(); // get all users which have permissions assigned to them
        }

        public void AddDevicePermission(string assignedUserId, int locationId, int deviceId)
        {
            LocationKey existingKey = SS.LocationKeyService.GetAll()
                .Where(lk => lk.OwnerId == assignedUserId && lk.LocationId == locationId)
                .SingleOrDefault();

            int locationKeyId = existingKey == null ? 0 : existingKey.Id;

            if (locationKeyId == 0)
            {
                LocationKey newKey = new LocationKey()
                {
                    LocationId = locationId,
                    OwnerId = assignedUserId,
                    HasFullDeviceControl = false,
                    Key = "SHOULD GET A KEY FROM THE PI HERE"
                };

                SS.LocationKeyService.Add(newKey);

                locationKeyId = newKey.Id;
            }

            DevicePermission permission = new DevicePermission()
            {
                DeviceId = deviceId,
                LocationKeyId = locationKeyId
            };

            bool permissionExists = GetAll()
                .Any(p => p.DeviceId == permission.DeviceId && p.LocationKeyId == permission.LocationKeyId);

            if (!permissionExists)
            {
                Add(permission);
            }

            throw new NotImplementedException("This method should be доработен такоа...");
        }

        public void AddLocationPermission(string assignedUserId, int locationId)
        {
            LocationKey existingKey = SS.LocationKeyService.GetAll()
                .Where(lk => lk.OwnerId == assignedUserId && lk.LocationId == locationId)
                .SingleOrDefault();

            if (existingKey == null)
            {
                string authKey;
                RemoteUser newRemoteUser = SS.RemoteIntegrationService.RegisterNewRemoteUser(assignedUserId, locationId, out authKey);

                if (string.IsNullOrEmpty(authKey)) return;

                LocationKey newKey = new LocationKey()
                {
                    LocationId = locationId,
                    OwnerId = assignedUserId,
                    HasFullDeviceControl = true,
                    Key = authKey
                };

                SS.RemoteUserService.Add(newRemoteUser, false);
                SS.LocationKeyService.Add(newKey);
            }
            else
            {
                if (!existingKey.HasFullDeviceControl)
                {
                    existingKey.HasFullDeviceControl = true;

                    SS.LocationKeyService.Update(existingKey);
                }
            }
        }

        public Dictionary<Location, IEnumerable<Device>> GetAssignedPermissionsForUser(string assignedUserId)
        {
            Dictionary<Location, IEnumerable<Device>> result = new Dictionary<Location, IEnumerable<Device>>();

            IQueryable<Location> allUserLocations = SS.LocationService.GetAll()
                .Where(l => l.OwnerId == CurrentUserId);

            IQueryable<LocationKey> assignedLocationsKeysForUser = allUserLocations
                .SelectMany(ul => ul.LocationKeys.Where(lk => lk.OwnerId == assignedUserId && lk.OwnerId != CurrentUserId));

            foreach (var locationKey in assignedLocationsKeysForUser.ToList())
            {
                if (locationKey.HasFullDeviceControl)
                {
                    result.Add(locationKey.Location, null);
                }
                else
                {
                    IEnumerable<Device> allowedDevices = locationKey.Location.Devices
                        .Where(d => d.Permissions
                            .Any(p => p.LocationKeyId == locationKey.Id));

                    if (allowedDevices.Count() == 0)
                    {
                        allowedDevices = null;
                    }

                    result.Add(locationKey.Location, allowedDevices);
                }
            }

            return result;
        }

        public void RevokeRightsForUser(string revokedUserId)
        {
            var currentUserId = CurrentUserId;

            var allDevicePermissions = GetAll().Where(dp => dp.LocationKey.OwnerId == revokedUserId && dp.Device.OwnerId == currentUserId).ToList();

            foreach (var devicePermission in allDevicePermissions)
            {
                Delete(devicePermission, false);
            }

            var locationKeysToDelete = SS.LocationService.GetAll()
                .Where(l => l.OwnerId == currentUserId)
                .SelectMany(l => l.LocationKeys)
                .Where(l => l.OwnerId == revokedUserId);

            foreach (var locationKey in locationKeysToDelete)
            {
                SS.LocationKeyService.Delete(locationKey, false);
            }

            SaveChanges();
        }

        public void RevokeDevicePermission(DevicePermission devicePermission, bool withSave = true)
        {
            Delete(devicePermission, withSave);
        }

        public void RevokeLocationKey(LocationKey locationKey, bool withSave = true)
        {
            SS.LocationKeyService.Delete(locationKey, withSave);
        }
    }
}
